import { createHmac } from 'crypto'
const dv = new DataView(new ArrayBuffer(8))
const __vr: (ReadonlyArray<number>)[] = []
const { floor, min, max } = Math
export type Secret = Buffer | NodeJS.TypedArray | DataView;

export const ms = 1_000
export const _31 = 2 ** 31 - 1
export const _32 = 2 ** -32

export const fullVerifiableRange: ReadonlyArray<number> = Object.freeze([0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15])
export const getVerifiableRange = (r: number) => __vr[r] || (__vr[r] = Object.freeze(fullVerifiableRange.slice(0, r)))
export const verifiableRange: ReadonlyArray<number> = getVerifiableRange(2)

export const truncate: ReadonlyArray<number> = Object.freeze([
	1,
	10,
	100,
	1_000,
	10_000,
	100_000,
	1_000_000,
	10_000_000,
	100_000_000,
	1_000_000_000
])

/**
 * @func getTOTP
 * @arg {buffer} secret binary key (required).
 * @arg {number} date time in milliseconds (optional, default `Date.now()`).
 * @arg {number} length of value to generate (optional, default `6`).
 * @arg {string} algorithm to digest with (optional, default `'sha1'`).
 * @arg {number} T0 as time offset in seconds (optional, default `0`).
 * @arg {number} TI as time increment in seconds (optional, default `30`).
 * @returns {number} signed positive 32 bit integer.
 *
 */
export function getTOTP(
	secret: Secret,
	T = Date.now(),
	length = 6,
	algorithm = 'sha1',
	T0 = 0,
	Tx = 30
): number {
	// calculate time counter CT using utime T - epoch T0; divised by duration Tx
	const CT = (floor(T / ms) - T0) / Tx

	// write time counter to a temporary buffer in big-endian mode
	dv.setUint32(0, CT * _32)
	dv.setUint32(4, CT)

	// generate hmac from big-endian counter time
	const HMAC = createHmac(algorithm, secret).update(dv).digest()
	// offset: last half byte value
	const OFF = HMAC[HMAC.length - 1] & 15
	const TOTPVal = (
		(HMAC[OFF + 0] << 24) |
		(HMAC[OFF + 1] << 16) |
		(HMAC[OFF + 2] << 8) |
		(HMAC[OFF + 3])
	) & _31 // number: read as Int32; but clear positive-negative indicator

	const TOTPValue = TOTPVal % truncate[length] // truncate to length
	return TOTPValue
}


export const getTOTPShort = (s: Secret, len = 6, alg = 'sha1') => getTOTP(s, Date.now(), len, alg)
export const getTOTPString = (s: Secret, len = 6, alg = 'sha1') => String(getTOTPShort(s, len, alg)).padStart(len, '0')
const NonDigits = /\D+/g

export function verifyTOTP (
	input: any,
	secret: Secret,
	range: number[] | number | ReadonlyArray<number> = verifiableRange,
	date = Date.now(),
	len = 6,
	alg = 'sha1',
	T0 = 0,
	TI = 30
): boolean {
	const V = 'number' !== typeof input
		? parseInt(String(input).replace(NonDigits, ''), 10)
		: floor(input) === input ? input : -1

	const R: number[] | ReadonlyArray<number> = 'number' === typeof range
		? getVerifiableRange(range)
		: range

	let M = 0, i = min(max(R.length, 0), 16) // maximum loops is 16, minimum is 0. Normal should be 2.
	while (i--) getTOTP(secret, date, len, alg, T0 + R[i] * TI, TI) === V ? ++M : null

	return M !== 0 // try to be time-constant
}
export {
	verifyTOTP as isValid,
	verifyTOTP as validate,
	verifyTOTP as getValidity
}

export {toURI, toOTPAuthURI, toURILong, toOTPAuthURILong, encSecret, baseURI, B32decode, TOTP_URI_Options} from './totp-uri'
