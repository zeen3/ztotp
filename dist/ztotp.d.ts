/// <reference types="node" />
export declare type Secret = Buffer | NodeJS.TypedArray | DataView;
export declare const ms = 1000;
export declare const _31: number;
export declare const _32: number;
export declare const fullVerifiableRange: ReadonlyArray<number>;
export declare const getVerifiableRange: (r: number) => ReadonlyArray<number>;
export declare const verifiableRange: ReadonlyArray<number>;
export declare const truncate: ReadonlyArray<number>;
export declare function getTOTP(secret: Secret, T?: number, length?: number, algorithm?: string, T0?: number, Tx?: number): number;
export declare const getTOTPShort: (s: Secret, len?: number, alg?: string) => number;
export declare const getTOTPString: (s: Secret, len?: number, alg?: string) => string;
export declare function verifyTOTP(input: any, secret: Secret, range?: number[] | number | ReadonlyArray<number>, date?: number, len?: number, alg?: string, T0?: number, TI?: number): boolean;
export { verifyTOTP as isValid, verifyTOTP as validate, verifyTOTP as getValidity };
export { toURI, toOTPAuthURI, toURILong, toOTPAuthURILong, encSecret, baseURI, B32decode, TOTP_URI_Options } from './totp-uri';
//# sourceMappingURL=ztotp.d.ts.map