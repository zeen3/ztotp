import { encode, decode } from 'buf-b32';
const baseURI = 'otpauth://totp/';
let warning_alg = 0, warning_epo = 0, warning_dig = 0, warning_per = 0;
const encSecret = (buf) => encode(buf, true)
    .replace(/=+/g, '');
function toURILong(key, keyEnc = 'buffer', acc = 'ERRACCOUTNAMEGOESHERE', iss = 'ERRSERVICEISSUERGOESHERE', len = 6, alg = 'sha1', T0 = 0, TI = 30) {
    const uri = new URL(baseURI + acc + ':' + iss), sp = uri.searchParams;
    switch (keyEnc) {
        case 'buffer':
            sp.set('secret', Buffer.isBuffer(key)
                ? encSecret(key)
                : 'string' === typeof key
                    ? key
                    : encSecret(key));
            break;
        default:
            sp.set('secret', String(key));
            break;
    }
    sp.set('issuer', iss);
    sp.set('account', acc);
    sp.set('algorithm', alg.toUpperCase());
    alg.toUpperCase() !== 'SHA1' && !warning_alg++ && process.emitWarning('[ztotp/totp-uri] Algorithms other than SHA1 may not be used by Google Authenticator; ensure your user has a working method and/or options to set algorithm.');
    sp.set('epoch', String(T0));
    T0 !== 0 && !warning_epo++ && process.emitWarning('[ztotp/totp-uri] An epoch other than 0 is mostly unsupported; ensure your user has a working method and/or options to change epoch.');
    sp.set('period', String(TI));
    TI !== 30 && !warning_per++ && process.emitWarning('[ztotp/totp-uri] Intervals other than 30 may not be used by Google Authenticator; ensure your user has a working method and/or options to set interval.');
    sp.set('digits', String(len));
    len !== 6 && !warning_dig++ && process.emitWarning('[ztotp/totp-uri] Lengths other than 6 may not be used by Google Authenticator; ensure your user has a working method and/or options to set length.');
    return uri.toString();
}
const toURI = ({ secret, secretEnc = 'string' === typeof secret ? 'string' : 'buffer', name = 'ERRACCOUNTNAMEGOESHERE', issuer = 'ERRSERVICEISSUERGOESHERE', length = 6, digits = length, len = digits, algorithm = 'sha1', digest = algorithm, alg = digest, epoch = 0, init = epoch, T0 = init, period = 30, steps = period, TI = steps }) => toURILong(secret, secretEnc, name, issuer, len, alg, T0, TI);
export default toURI;
export { toURI, toURI as toOTPAuthURI, toURILong, toURILong as toOTPAuthURILong, encSecret, baseURI, decode as B32decode };
//# sourceMappingURL=totp-uri.mjs.map