(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define('ztotp', ["require", "exports", "crypto", "./totp-uri"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    const crypto_1 = require("crypto");
    const dv = new DataView(new ArrayBuffer(8));
    const __vr = [];
    const { floor, min, max } = Math;
    exports.ms = 1000;
    exports._31 = 2 ** 31 - 1;
    exports._32 = 2 ** -32;
    exports.fullVerifiableRange = Object.freeze([0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15]);
    exports.getVerifiableRange = (r) => __vr[r] || (__vr[r] = Object.freeze(exports.fullVerifiableRange.slice(0, r)));
    exports.verifiableRange = exports.getVerifiableRange(2);
    exports.truncate = Object.freeze([
        1,
        10,
        100,
        1000,
        10000,
        100000,
        1000000,
        10000000,
        100000000,
        1000000000
    ]);
    function getTOTP(secret, T = Date.now(), length = 6, algorithm = 'sha1', T0 = 0, Tx = 30) {
        const CT = (floor(T / exports.ms) - T0) / Tx;
        dv.setUint32(0, CT * exports._32);
        dv.setUint32(4, CT);
        const HMAC = crypto_1.createHmac(algorithm, secret).update(dv).digest();
        const OFF = HMAC[HMAC.length - 1] & 15;
        const TOTPVal = ((HMAC[OFF + 0] << 24) |
            (HMAC[OFF + 1] << 16) |
            (HMAC[OFF + 2] << 8) |
            (HMAC[OFF + 3])) & exports._31;
        const TOTPValue = TOTPVal % exports.truncate[length];
        return TOTPValue;
    }
    exports.getTOTP = getTOTP;
    exports.getTOTPShort = (s, len = 6, alg = 'sha1') => getTOTP(s, Date.now(), len, alg);
    exports.getTOTPString = (s, len = 6, alg = 'sha1') => String(exports.getTOTPShort(s, len, alg)).padStart(len, '0');
    const NonDigits = /\D+/g;
    function verifyTOTP(input, secret, range = exports.verifiableRange, date = Date.now(), len = 6, alg = 'sha1', T0 = 0, TI = 30) {
        const V = 'number' !== typeof input
            ? parseInt(String(input).replace(NonDigits, ''), 10)
            : floor(input) === input ? input : -1;
        const R = 'number' === typeof range
            ? exports.getVerifiableRange(range)
            : range;
        let M = 0, i = min(max(R.length, 0), 16);
        while (i--)
            getTOTP(secret, date, len, alg, T0 + R[i] * TI, TI) === V ? ++M : null;
        return M !== 0;
    }
    exports.verifyTOTP = verifyTOTP;
    exports.isValid = verifyTOTP;
    exports.validate = verifyTOTP;
    exports.getValidity = verifyTOTP;
    var totp_uri_1 = require("./totp-uri");
    exports.toURI = totp_uri_1.toURI;
    exports.toOTPAuthURI = totp_uri_1.toOTPAuthURI;
    exports.toURILong = totp_uri_1.toURILong;
    exports.toOTPAuthURILong = totp_uri_1.toOTPAuthURILong;
    exports.encSecret = totp_uri_1.encSecret;
    exports.baseURI = totp_uri_1.baseURI;
    exports.B32decode = totp_uri_1.B32decode;
});
//# sourceMappingURL=ztotp.js.map