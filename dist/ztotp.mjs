import { createHmac } from 'crypto';
const dv = new DataView(new ArrayBuffer(8));
const __vr = [];
const { floor, min, max } = Math;
export const ms = 1000;
export const _31 = 2 ** 31 - 1;
export const _32 = 2 ** -32;
export const fullVerifiableRange = Object.freeze([0, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15]);
export const getVerifiableRange = (r) => __vr[r] || (__vr[r] = Object.freeze(fullVerifiableRange.slice(0, r)));
export const verifiableRange = getVerifiableRange(2);
export const truncate = Object.freeze([
    1,
    10,
    100,
    1000,
    10000,
    100000,
    1000000,
    10000000,
    100000000,
    1000000000
]);
export function getTOTP(secret, T = Date.now(), length = 6, algorithm = 'sha1', T0 = 0, Tx = 30) {
    const CT = (floor(T / ms) - T0) / Tx;
    dv.setUint32(0, CT * _32);
    dv.setUint32(4, CT);
    const HMAC = createHmac(algorithm, secret).update(dv).digest();
    const OFF = HMAC[HMAC.length - 1] & 15;
    const TOTPVal = ((HMAC[OFF + 0] << 24) |
        (HMAC[OFF + 1] << 16) |
        (HMAC[OFF + 2] << 8) |
        (HMAC[OFF + 3])) & _31;
    const TOTPValue = TOTPVal % truncate[length];
    return TOTPValue;
}
export const getTOTPShort = (s, len = 6, alg = 'sha1') => getTOTP(s, Date.now(), len, alg);
export const getTOTPString = (s, len = 6, alg = 'sha1') => String(getTOTPShort(s, len, alg)).padStart(len, '0');
const NonDigits = /\D+/g;
export function verifyTOTP(input, secret, range = verifiableRange, date = Date.now(), len = 6, alg = 'sha1', T0 = 0, TI = 30) {
    const V = 'number' !== typeof input
        ? parseInt(String(input).replace(NonDigits, ''), 10)
        : floor(input) === input ? input : -1;
    const R = 'number' === typeof range
        ? getVerifiableRange(range)
        : range;
    let M = 0, i = min(max(R.length, 0), 16);
    while (i--)
        getTOTP(secret, date, len, alg, T0 + R[i] * TI, TI) === V ? ++M : null;
    return M !== 0;
}
export { verifyTOTP as isValid, verifyTOTP as validate, verifyTOTP as getValidity };
export { toURI, toOTPAuthURI, toURILong, toOTPAuthURILong, encSecret, baseURI, B32decode } from './totp-uri.mjs';
//# sourceMappingURL=ztotp.mjs.map